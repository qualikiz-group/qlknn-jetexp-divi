""" Runs the Python version of qlknn-jetexp """
from pathlib import Path
import re

# Use the QLKNN classes https://gitlab.com/Karel-van-de-Plassche/QLKNN-develop
from qlknn.models.committee import (
    QuaLiKizNDNN,
    QuaLiKizNDNNCollection,
    QuaLiKizCommitteeNN,
    QuaLiKizCommitteeProductNN,
)


nn = None
npath = Path(".")
if npath.is_dir():
    pure_nn_bases = []
    impure_nn_bases = []
    for nn_path in npath.glob("*.json"):
        mm = re.match(r"^(.+)_0*1.json$", str(nn_path.name))
        if mm:
            if re.search("_div_", mm.group(1)):
                impure_nn_bases.append(mm.group(1))
            else:
                pure_nn_bases.append(mm.group(1))
    nns = {}
    for nn_base in pure_nn_bases:
        nn_list = [
            QuaLiKizNDNN.from_json(str(nn_path.resolve()))
            for nn_path in npath.glob(nn_base + "*.json")
        ]
        nn_comm = QuaLiKizCommitteeNN(nn_list, low_bound=0.0)
        nns[nn_comm._target_names[0]] = nn_comm
    for nn_base in impure_nn_bases:
        nn_list = [
            QuaLiKizNDNN.from_json(str(nn_path.resolve()))
            for nn_path in npath.glob(nn_base + "*.json")
        ]
        nn_comm = QuaLiKizCommitteeNN(nn_list)
        mmm = re.match(r"^(.+)_div_(.+)$", nn_comm._target_names[0])
        if mmm and mmm.group(2) in nns:
            nn_comm = QuaLiKizCommitteeProductNN(
                [mmm.group(1), mmm.group(1) + "_EB"], [nns[mmm.group(2)], nn_comm]
            )
        nns[nn_comm._target_names[0]] = nn_comm
    nn = QuaLiKizNDNNCollection(list(nns.values()))


nns = {"qlknn-jetexp": nn}
slicedim = "Ati"
style = "mono"

if __name__ == "__main__":
    from IPython import embed  # pylint: disable=unused-import # noqa: F401
    import pandas as pd
    import numpy as np

    inp = pd.DataFrame()
    scann = 100

    data = {
        "Ane": 2.0,
        "x": 0.55,
        "Ate": 5.5,
        "gammaE": -0.2,
        "q": 1.8,
        "Machtor": 0.3,
        "Autor": 1.5,
        "alpha": 0.3,
        "Zeff": 1.7,
        "smag": 0.8,
        "Ani1": 2.0,
        "normni1": 0.017,
        "Ati0": np.linspace(-2.0, 18, 11),
        "Ti_Te0": 1.0,
        "logNustar": -0.9,
    }
    inp = pd.DataFrame(data)
    extra_options = {"clip_low": True, "clip_x": True}
    outp = nn.get_output(inp, output_pandas=True, safe=True, **extra_options)
    print(outp)
